package com.vrapalis.www.apidesignfirst.domain.user;

import com.vrapalis.www.apidesignfirst.api.UserApi;
import com.vrapalis.www.apidesignfirst.model.PostUser;
import com.vrapalis.www.apidesignfirst.model.User;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("${api-base-path}")
public class UserRestController implements UserApi {

  private Map<UUID, User> users = new HashMap<>();

  public UserRestController() {
    var keyOne = UUID.randomUUID();
    users.put(keyOne, new User().id(keyOne.toString()).name("Mike"));
    var keyTwo = UUID.randomUUID();
    users.put(keyTwo, new User().id(keyTwo.toString()).name("Dike"));
  }

  @Override
  public ResponseEntity<User> createUser(PostUser postUser) {
    return ResponseEntity.ok(new User().id(UUID.randomUUID().toString()).name(postUser.getName()));
  }

  @Override
  public ResponseEntity<Void> deleteUser(String userId) {
    return ResponseEntity.ok().build();
  }

  @Override
  public ResponseEntity<User> getUserById(String userId) {
    var foundUser = users.get(UUID.fromString(userId));
    return foundUser != null ? ResponseEntity.ok(foundUser) : ResponseEntity.notFound().build();
  }

  @Override
  public ResponseEntity<List<User>> getUsers() {
    return ResponseEntity.ok(new ArrayList<>(users.values()));
  }

  @Override
  public ResponseEntity<Void> updateUser(String userId, Optional<User> user) {
    return ResponseEntity.ok().build();
  }
}
